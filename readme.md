## How to Running 
### Tampilan website

---
- Buka terminal pada vs code
- Lalu ketik

```python
python manage.py runserver
```

Klik link yang muncul, lalu jika terdapat tulisan "Page Not Found" ketikkan

```
http://127.0.0.1:8000/test/
```
Tampilan website akan muncul.


### Admin panel

- Caranya sama dengan membuka tampilan website hanya saja Admin panel menggunakan
```
http://127.0.0.1:8000/admin/
