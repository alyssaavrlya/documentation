from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.template.defaultfilters import slugify

class Documentation(models.Model):
    title = models.CharField(max_length=255)
    content = RichTextUploadingField()
    table_of_contents = RichTextUploadingField()
    slug = models.SlugField(null=True, blank=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)

        super(Documentation, self).save(args, kwargs)