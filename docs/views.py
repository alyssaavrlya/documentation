from django.shortcuts import render
from .models import Documentation
from django.shortcuts import render, get_object_or_404

# Create your views here.

def view_docs(request, slug):
    model = get_object_or_404(Documentation, slug=slug)

    return render(request, "index.html", {"docs": model})

    