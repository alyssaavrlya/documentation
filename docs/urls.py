from django.contrib import admin
from django.urls import path, include, re_path

from .views import *

urlpatterns = [
    path('/<slug:slug>/', view_docs),
]